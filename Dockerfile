FROM java:8-jre-alpine

EXPOSE 8761

ADD ./target/bestshop-registry-service.jar /app/

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/bestshop-registry-service.jar"]